import React from "react";
import FollowersIcon from "../../atoms/Icons/FollowersIcon";
import FollowingIcon from "../../atoms/Icons/FollowingIcon";

type Props = {
  html_url: string;
  avatar_url: string;
  name: string;
  login: string;
  followers: number;
  following: number;
};

const comfortView = (followers: number, following: number) => {
  let strFollowers: string | string[] = followers.toString();
  let strFollowing: string | string[] = following.toString();
  switch (strFollowers.length) {
    case 4:
      strFollowers = strFollowers.split("").splice(0, 2);
      strFollowers.splice(1, 0, ".");
      strFollowers.push("k");
      break;
    case 5:
      strFollowers = strFollowers.split("").splice(0, 3);
      strFollowers.splice(2, 0, ".");
      strFollowers.push("k");
      break;
    case 6:
      strFollowers = strFollowers.split("").splice(0, 4);
      strFollowers.splice(3, 0, ".");
      strFollowers.push("k");
      break;
    default:
      break;
  }
  switch (strFollowing.length) {
    case 4:
      strFollowing = strFollowing.split("").splice(0, 2);
      strFollowing.splice(1, 0, ".");
      strFollowing.push("k");
      break;
    case 5:
      strFollowing = strFollowing.split("").splice(0, 3);
      strFollowing.splice(2, 0, ".");
      strFollowing.push("k");
      break;
    case 6:
      strFollowing = strFollowing.split("").splice(0, 4);
      strFollowing.splice(3, 0, ".");
      strFollowing.push("k");
      break;
    default:
      break;
  }
  return [strFollowers, strFollowing];
};

const UserInfo = ({
  avatar_url,
  name,
  login,
  html_url,
  followers,
  following,
}: Props) => {
  const [strFollowers, strFollowing] = comfortView(followers, following);

  return (
    <div className="flex flex-col basis-[280px] shrink-0">
      <img
        src={avatar_url}
        alt="Icon"
        width={280}
        height={280}
        className="rounded-[50%]"
      />
      <span className="mt-[29px] text-[26px] font-semibold leading-[33.80px]">
        {name}
      </span>
      <a
        href={html_url}
        target="_blank"
        rel="noreferrer"
        className="text-blue text-lg"
      >
        {login}
      </a>
      <div className="flex justify-between items-center mt-[25px] text-grey">
        <div className="flex items-center">
          <FollowersIcon width={24} height={24} className="mr-[7px]" />
          {strFollowers} followers
        </div>
        <div className="flex items-center">
          <FollowingIcon width={24} height={24} className="mr-[7px]" />
          {strFollowing} following
        </div>
      </div>
    </div>
  );
};

export default UserInfo;
