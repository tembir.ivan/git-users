import React, { useEffect, useState } from "react";
import GitIcon from "../../atoms/Icons/GitIcon";
import SearchIcon from "../../atoms/Icons/SearchIcon";
import { useAppDispatch } from "../../../../../redux/hooks";
import { setUser } from "../../../../../redux/features/userSlice";

const Header = () => {
  const [value, setValue] = useState<string>();
  const dispatch = useAppDispatch();

  useEffect(() => {
    let check: boolean = false;
    if (value !== ("" || undefined)) {
      const userPromise = new Promise((resolve) => {
        fetch(`https://api.github.com/users/${value}`)
          .then((response) => response.json())
          .then((data) => resolve(data));
      });

      const reposePromise = new Promise((resolve) => {
        fetch(`https://api.github.com/users/${value}/repos`)
          .then((response) => response.json())
          .then((data) => resolve(data));
      });

      Promise.all([userPromise, reposePromise]).then((result) => {
        if (check === false) dispatch(setUser(result));
      });
    }
    return () => {
      check = true;
    };
  }, [value]);

  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  return (
    <header className="bg-blue">
      <div className="flex items-center w-full py-4 px-14">
        <a href="https://github.com/" target="_blank" rel="noreferrer">
          <GitIcon width={41} height={40} />
        </a>
        <div className="relative flex items-center ml-[22px]">
          <SearchIcon
            width={44}
            height={24}
            className="absolute left-0 cursor-pointer"
          />
          <input
            type="text"
            name="search"
            className="w-[500px] h-10 pr-[14px] pl-[44px] py-2 bg-white border-none outline-none rounded-md text-sm tracking-tight"
            value={value}
            onChange={changeHandler}
          />
        </div>
      </div>
    </header>
  );
};

export default Header;
