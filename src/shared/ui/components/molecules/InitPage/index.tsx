import React from "react";
import SearchIcon from "../../atoms/Icons/SearchIcon";

const InitPage = () => {
  return (
    <div className="flex flex-col items-center justify-center w-full h-full">
      <SearchIcon width={110} height={110} />
      <p className="max-w-[210px] text-center text-zinc-500 text-[22px] sleading-[30.80px] mt-6 text-grey">
        Start with searching a GitHub user
      </p>
    </div>
  );
};

export default InitPage;
