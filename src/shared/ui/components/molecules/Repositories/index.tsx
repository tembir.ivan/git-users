import React, { useState } from "react";
import { useAppSelector } from "../../../../../redux/hooks";
import EmptyRepos from "../EmptyRepos";
import RepositoryBox from "../../atoms/RepositoryBox";
import Pagination from "../../atoms/Pagination";

const Repositories = () => {
  const [page, setPage] = useState<number>(1);
  const repositories = useAppSelector((state) => state.user.repos);

  const pageHandler = (value: number) => {
    setPage(value);
  };

  if (Array.isArray(repositories))
    return (
      <div className="flex flex-col px-20 basis-full">
        <span className="text-[32px] font-semibold leading-[41.60px]">
          Repositories ({repositories.length})
        </span>
        <div className="flex flex-col flex-1 mt-[29px]">
          {repositories.map((elem, index) => {
            const newIndex = index + 1;
            if ((page - 1) * 4 + 1 <= newIndex && page * 4 >= newIndex) {
              return <RepositoryBox {...elem} />;
            }
          })}
        </div>
        <Pagination
          amount={repositories.length}
          page={page}
          changePage={pageHandler}
        />
      </div>
    );
  else return <EmptyRepos />;
};

export default Repositories;
