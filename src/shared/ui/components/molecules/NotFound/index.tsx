import React from "react";
import UserIcon from "../../atoms/Icons/UserIcon";

const NotFound = () => {
  return (
    <div className="flex flex-col items-center justify-center w-full h-full">
      <UserIcon width={110} height={110} />
      <p className="text-zinc-500 text-[22px] sleading-[30.80px] mt-6 text-grey">
        User Not Found
      </p>
    </div>
  );
};

export default NotFound;
