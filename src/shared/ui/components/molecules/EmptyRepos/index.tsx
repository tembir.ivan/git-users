import React from "react";
import RepositoryIcon from "../../atoms/Icons/RepositoryIcon";

const EmptyRepos = () => {
  return (
    <div className="flex flex-col items-center justify-center w-full h-full">
      <RepositoryIcon width={110} height={110} />
      <p className="text-center text-zinc-500 text-[22px] sleading-[30.80px] mt-6 text-grey">
        Repository list is empty
      </p>
    </div>
  );
};

export default EmptyRepos;
