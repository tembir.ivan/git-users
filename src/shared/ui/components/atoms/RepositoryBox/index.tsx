import React from "react";

type Props = {
  name: string;
  description: string;
  url: string;
};

const RepositoryBox = ({ name, description, url }: Props) => {
  return (
    <div className="w-full py-6 px-8 flex flex-col mt-6 first:mt-0 bg-white">
      <a
        href={url}
        target="_blank"
        rel="noreferrer"
        className="text-blue text-2xl font-medium"
      >
        {name}
      </a>
      <p className="mt-4">{description}</p>
    </div>
  );
};

export default RepositoryBox;
