import React, { useState } from "react";
import Arrow from "../Icons/Arrow";

type Props = {
  page: number;
  amount: number;
  changePage: (page: number) => void;
};

const PageElem = ({
  page,
  index,
  newIndex,
  clickHandler,
}: {
  page: number;
  index: number;
  newIndex: number;
  clickHandler: () => void;
}) => (
  <span
    onClick={clickHandler}
    className={`cursor-pointer flex text-sm leading-[21px] w-[21px] h-[25px] text-grey justify-center items-center ${
      page - 1 === index && "text-white bg-blue rounded-[3px]"
    }`}
  >
    {newIndex}
  </span>
);
const Pagination = ({ amount, changePage, page }: Props) => {
  const pages = new Array(Math.ceil(amount / 4)).fill("");
  const memory: [boolean, boolean] = [false, false];

  return (
    <div className="flex flex-grow-0 flex-shrink-0 items-center self-end mt-4">
      <span className="text-sm leading-[21px] text-grey">
        {(page - 1) * 4 + 1}-
        {Math.ceil(amount / 4) === page ? <>{amount}</> : <>{page * 4}</>} of{" "}
        {amount}
      </span>
      <Arrow
        width={18}
        height={18}
        onClick={() => {
          if (page !== 1) {
            changePage(page - 1);
          }
        }}
        className="cursor-pointer ml-[19px]"
      />
      {pages.map((value, index) => {
        const newIndex = index + 1;
        if (amount > 5) {
          if (newIndex === page)
            return (
              <PageElem
                key={index}
                page={page}
                index={index}
                newIndex={newIndex}
                clickHandler={() => {
                  changePage(newIndex);
                }}
              />
            );

          if (newIndex === 1)
            return (
              <PageElem
                key={index}
                page={page}
                index={index}
                newIndex={newIndex}
                clickHandler={() => {
                  changePage(newIndex);
                }}
              />
            );
          if (newIndex === pages.length)
            return (
              <PageElem
                key={index}
                page={page}
                index={index}
                newIndex={newIndex}
                clickHandler={() => {
                  changePage(newIndex);
                }}
              />
            );
          if (newIndex > 1 && newIndex < pages.length) {
            if (newIndex + 1 === page || newIndex - 1 === page)
              return (
                <PageElem
                  key={index}
                  page={page}
                  index={index}
                  newIndex={newIndex}
                  clickHandler={() => {
                    changePage(newIndex);
                  }}
                />
              );
            if (newIndex > page && !memory[1]) {
              memory[1] = true;
              return (
                <span
                  key={index}
                  className={`flex text-sm leading-[21px] w-[21px] h-[25px] text-grey justify-center items-center `}
                >
                  ...
                </span>
              );
            }
            if (newIndex < page && !memory[0]) {
              memory[0] = true;
              return (
                <span
                  key={index}
                  className={`flex text-sm leading-[21px] w-[21px] h-[25px] text-grey justify-center items-center `}
                >
                  ...
                </span>
              );
            }
          }
        } else
          return (
            <PageElem
              key={index}
              page={page}
              index={index}
              newIndex={newIndex}
              clickHandler={() => {
                changePage(newIndex);
              }}
            />
          );
      })}
      <Arrow
        className="cursor-pointer rotate-180 ml-[19px]"
        width={18}
        height={18}
        onClick={() => {
          if (page !== pages.length) {
            changePage(page + 1);
          }
        }}
      />
    </div>
  );
};

export default Pagination;
