import React from "react";
import NotFound from "../../molecules/NotFound";
import InitPage from "../../molecules/InitPage";
import UserInfo from "../../molecules/UserInfo";
import { useAppSelector } from "../../../../../redux/hooks";
import Repositories from "../../molecules/Repositories";

const User = () => {
  const userInfo = useAppSelector((state) => state.user.user);
  console.log(userInfo);
  return (
    <main>
      <section>
        <div className="py-[28px] px-14 min-h-calc flex bg-[#F9F9F9]">
          {userInfo.message ? (
            <NotFound />
          ) : (
            <>
              {userInfo.login !== undefined ? (
                <>
                  <UserInfo {...userInfo} />
                  <Repositories />
                </>
              ) : (
                <InitPage />
              )}
            </>
          )}
        </div>
      </section>
    </main>
  );
};

export default User;
