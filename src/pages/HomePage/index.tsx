import React from "react";
import Header from "../../shared/ui/components/molecules/Header";
import User from "../../shared/ui/components/organisms/User";

const HomePage = () => {
  return (
    <>
      <Header />
      <User />
    </>
  );
};

export default HomePage;
