import { createSlice } from "@reduxjs/toolkit";

type UserSlice = {
  user: {
    html_url: string;
    avatar_url: string;
    name: string;
    login: string;
    followers: number;
    following: number;
    [x: string]: any;
  };
  repos: any[] | {};
};

const initialState: UserSlice = {
  user: {} as any,
  repos: [],
};

export const userSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload[0];
      state.repos = action.payload[1];
    },
  },
});

export const { setUser } = userSlice.actions;

export default userSlice.reducer;
