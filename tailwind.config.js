/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      blue: "#0064EB",
      black: "#000000",
      white: "#FFFFFF",
      grey: "#808080",
      background: "#E5E5E5",
    },
    extend: { minHeight: { calc: "calc(100vh - 72px)" } },
  },
  plugins: [],
};
